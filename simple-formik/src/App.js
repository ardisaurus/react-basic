import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import TabelHalaman from "./pages/TabelHalaman";
import LoginPlain from "./pages/LoginPlain";
import LoginFormik from "./pages/LoginFormik";
import GenericForm from "./pages/GenericForm";

function App() {

  return (
    <Router>
      <Route exact path="/" component={TabelHalaman} />
      <Route path="/login-plain" component={LoginPlain} />
      <Route path="/login-formik" component={LoginFormik} />
      <Route path="/generic-formik" component={GenericForm} />
    </Router>
  );
}

export default App;
