import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import Checkbox from "@material-ui/core/Checkbox";
import { withStyles } from "@material-ui/core/styles";
import { blue } from "@material-ui/core/colors";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import { useFormik } from "formik";

const BlueCheckbox = withStyles({
  root: {
    color: blue[300],
    "&$checked": {
      color: blue[600],
    },
  },
  checked: {},
})((props) => <Checkbox color="default" {...props} />);

export default function LoginPlain() {
  const formik = useFormik({
    initialValues: {
      text: "",
      password: "",
      number: "",
      checkbox: "alpha",
      select: false,
      date: null,
    },
    onSubmit: (values) => {
      console.log(values);
    },
  });

  return (
    <div>
      <form onSubmit={formik.handleSubmit}>
        {/* Input text */}
        <TextField
          fullWidth
          name="text"
          label="text"
          value={formik.text}
          onChange={formik.handleChange}
        />
        {/* Input password */}
        <TextField
          fullWidth
          name="password"
          label="password"
          type="password"
          value={formik.password}
          onChange={formik.handleChange}
        />
        {/* Input number */}
        <TextField
          fullWidth
          name="number"
          label="number"
          type="number"
          value={formik.number}
          onChange={formik.handleChange}
        />
        {/* Input checkbox */}
        <FormControlLabel
          control={
            <BlueCheckbox
              name="checkbox"
              checked={formik.checkbox}
              onChange={formik.handleChange}
            />
          }
          label="Checkbox"
        />
        <br />
        {/* Input select */}
        <FormControl variant="outlined">
          <InputLabel htmlFor="outlined-age-native-simple">Select</InputLabel>
          <Select
            native
            label="Select"
            name="select"
            value={formik.select}
            onChange={formik.handleChange}
          >
            <option value="alpha">alpha</option>
            <option value="beta">beta</option>
            <option value="gamma">gamma</option>
          </Select>
        </FormControl>
        {/* Input datePicker */}
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            name="date"
            label="date"
            variant="outlined"
            format="dd/MM/yyyy"
            KeyboardButtonProps={{
              "aria-label": "change date",
            }}
            value={formik.values.date}
            onChange={(val) => {
              formik.setFieldValue("date", val);
            }}
            fullWidth
          />
        </MuiPickersUtilsProvider>

        <Button color="primary" variant="contained" fullWidth type="submit">
          Submit
        </Button>
      </form>
    </div>
  );
}
