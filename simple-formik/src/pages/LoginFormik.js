import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
//1. import useFormik dari formik
import { useFormik } from 'formik';

export default function LoginFormik() {
    //2. buat hook formik
    const formik = useFormik({
        //3. buat initial values
        initialValues: {
            email: '',
            password: '',
        },
        //4. fungsi submit
        onSubmit: (values) => {
            console.log(values);
        },
    });

    return (
        <div>
            {/* 5. hubungkan event onSubmit dari form ke submitHandler formik */}
            <form onSubmit={formik.handleSubmit}>
                <TextField
                    fullWidth
                    // 6. beri nama pada komponen input
                    name="email"
                    label="Email"
                    // 6. hubungkan value dan onChange event dengan formik
                    value={formik.values.email}
                    onChange={formik.handleChange}
                /><TextField
                    fullWidth
                    // 6. beri nama pada komponen input
                    name="password"
                    label="Password"
                    type="password"
                    // 6. hubungkan value dan onChange event dengan formik
                    value={formik.values.password}
                    onChange={formik.handleChange}
                />
                <Button color="primary" variant="contained" fullWidth type="submit">
                    Submit
                </Button>
            </form>
        </div>
    )
}
