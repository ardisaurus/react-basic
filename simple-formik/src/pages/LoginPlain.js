import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

export default function LoginPlain() {

    return (
        <div>
            <form>
                <TextField
                    fullWidth
                    name="email"
                    label="Email"
                />
                <TextField
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                />
                <Button color="primary" variant="contained" fullWidth type="submit">
                    Submit
                </Button>
            </form>
        </div>
    )
}
