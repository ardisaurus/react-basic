import React from 'react'
import { Link } from 'react-router-dom'


export default function TabelHalaman() {
    return (
        <div>
            <table>
                <tbody>
                    <tr>
                        <td>Login Plain</td>
                        <td><Link to="login-plain">Buka</Link></td>
                    </tr>
                    <tr>
                        <td>Login Formik</td>
                        <td><Link to="login-formik">Buka</Link></td>
                    </tr>
                    <tr>
                        <td>Generic Formik</td>
                        <td><Link to="generic-formik">Buka</Link></td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}
