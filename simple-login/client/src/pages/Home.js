import React, { useState } from "react";
import authServices from "../services/auth.service";
import helloServices from "../services/hello.service";
import { useNavigate } from "react-router-dom";

export default function Home() {
  const navigate = useNavigate();
  const [response, setResponse] = useState({});
  const logout = async () => {
    await authServices.logout();
    navigate("/");
  };
  const helloWorld = async () => {
    const res = await helloServices.helloWorld();
    setResponse(res);
  };
  return (
    <div>
      Welcome
      <br />
      <div style={{ border: "solid red 1px", padding: "1em" }}>
        {JSON.stringify(response)}
        <button onClick={helloWorld}>Get API Response</button>
      </div>
      <button onClick={logout}>Log Out</button>
    </div>
  );
}
