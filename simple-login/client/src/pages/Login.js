import { useFormik } from "formik";
import React, { useState } from "react";
import * as Yup from "yup";
import authServices from "../services/auth.service";
import { useNavigate } from "react-router-dom";

const validationSchema = Yup.object().shape({
  username: Yup.string().required("Insert username"),
  password: Yup.string()
    .min(5, "Use combination of 6 character or more")
    .required("Insert password"),
  rememberMe: Yup.boolean(),
});

function Login() {
  const navigate = useNavigate();
  const [errorMsg, setErrorMsg] = useState("");
  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
      rememberMe: false,
    },
    validationSchema: validationSchema,
    validateOnBlur: false,
    validateOnChange: false,
    onSubmit: async ({ username, password, rememberMe }) => {
      // console.log({ username, password, rememberMe });
      const result = await authServices.login({
        username,
        password,
        rememberMe,
      });
      if (!Boolean(result.error)) {
        navigate("/home");
      } else {
        setErrorMsg(result.error.response.data.error.message);
      }
    },
  });

  return (
    <div>
      <h1>Login</h1>
      {Boolean(errorMsg) && errorMsg}
      <form
        onSubmit={(e) => {
          e.preventDefault();
          formik.handleSubmit();
        }}
      >
        {/* Username Input */}
        <label htmlFor="username">Username:</label>
        <br />
        <input
          type="text"
          name="username"
          value={formik.values.username}
          onChange={formik.handleChange}
        />
        <br />
        {Boolean(formik.errors.username) && formik.touched.username
          ? "* " + formik.errors.username
          : ""}
        <br />
        {/* Password Input */}
        <label htmlFor="password">Password:</label>
        <br />
        <input
          type="password"
          name="password"
          value={formik.values.password}
          onChange={formik.handleChange}
        />
        <br />
        {Boolean(formik.errors.password) && formik.touched.password
          ? "* " + formik.errors.password
          : ""}
        <br />
        {/* remember me Input */}
        <input
          type="checkbox"
          name="rememberMe"
          checked={formik.values.rememberMe}
          onChange={(e) => {
            formik.setValues({
              ...formik.values,
              rememberMe: e.target.checked,
            });
          }}
        />
        <label htmlFor="rememberMe">Remember Me</label>
        <br />
        {/* Submit Button */}
        <input type="submit" value="Submit" />
      </form>
      <pre>username : admin | password : admin</pre>
    </div>
  );
}

export default Login;
