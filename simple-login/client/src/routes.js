import { Navigate } from "react-router-dom";
import Login from "./pages/Login";
import Home from "./pages/Home";

const routes = (isLoggedIn) => [
  {
    // protected routes
    path: "/home",
    element: isLoggedIn ? <Home /> : <Navigate to="/" />,
  },
  {
    // public routes
    path: "/",
    element: !isLoggedIn ? <Login /> : <Navigate to="/home" />,
  },
];

export default routes;
