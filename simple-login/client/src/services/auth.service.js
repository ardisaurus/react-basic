import axios from "axios";
import jwt_decode from "jwt-decode";
import AuthToken from "../utils/auth-token";

const apiUrl = process.env.REACT_APP_API_BASEURL;

class AuthService {
  async login({ username, password, rememberMe }) {
    try {
      const response = await axios.post(apiUrl + "/login", {
        username,
        password,
      });
      //   console.log({ response });
      if (response.data && response.data.token) {
        // console.log(decoded);
        const token = response.data.token;
        const { exp } = jwt_decode(token);
        AuthToken.setToken(token, exp, rememberMe);
        return response.data;
      }
    } catch (error) {
      console.log({ error });
      return { error };
    }
  }

  logout() {
    AuthToken.removeToken();
  }

  getLoginStatus() {
    const token = AuthToken.getToken();
    if (Boolean(token)) {
      return true;
    } else {
      return false;
    }
  }
}

export default new AuthService();
