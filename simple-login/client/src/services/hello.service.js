import axios from "axios";
import authHeader from "../utils/auth-header";
import AuthToken from "../utils/auth-token";

const apiUrl = process.env.REACT_APP_API_BASEURL;

class HelloService {
  async helloWorld() {
    try {
      const response = await axios.get(apiUrl + "/api/hello-world", {
        headers: authHeader(),
      });
      //   console.log({ response });
      if (response.data) {
        return response.data;
      }
    } catch (error) {
      // console.error(error);
      if (error.response.status === 401) {
        AuthToken.removeToken();
        window.location = "/";
      }
      return { error };
    }
  }
}

export default new HelloService();
