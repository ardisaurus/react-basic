import AddTodo from "./components/AddTodo";
import TodoList from "./components/TodoList";
import "./App.css";
import { useQuery, useMutation, useQueryClient } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import { getTodos, addTodos, removeTodos } from './services/TodoApi';
import { v4 as uuid } from "uuid";

function App() {

  const queryClient = useQueryClient();

  const { isLoading, isError, isSuccess, error, data } = useQuery('todoData', () => getTodos());

  const markComplete = (id) => {
    const res = data.map((todo) => {
      if (todo.id === id) {
        todo.completed = !todo.completed;
      }
      return todo;
    });
    queryClient.setQueriesData('todoData', res);
  };

  const mutatePostTodo = useMutation(
    values => addTodos({ id: uuid(), ...values }),
    {
      onSuccess: (res) => {
        // Query Invalidations
        // queryCache.invalidateQueries('todoData')
        queryClient.setQueriesData('todoData', [res, ...data]);
      },
    }
  )

  const mutateDeleteTodo = useMutation(
    id => removeTodos(id), {
    onSuccess: (res, variables) => {
      // Query Invalidations
      // queryCache.invalidateQueries('todoData')
      const newTodo = data.filter((todo) => todo.id !== variables);
      queryClient.setQueriesData('todoData', newTodo);
    }
  }
  )

  const addTodo = (title) => mutatePostTodo.mutate({
    title,
    completed: false,
  })

  const delTodo = (id) => { mutateDeleteTodo.mutate(id) }

  return (
    <div className="App">
      <h1>Todo List</h1>
      {/* Data fetching props */}
      {isLoading && "Loading..."}
      {isError && (<pre>{error.message}</pre>)}
      {isSuccess && (
        <div className="container">
          <AddTodo addTodo={addTodo} />
          {/* Data add data props */}
          {mutatePostTodo.isLoading && "Adding data on progress.."}
          {mutatePostTodo.isError && (<div className='message'>{mutatePostTodo.error.message}</div>)}
          {mutatePostTodo.isSuccess && (<div className='message'>Data Added</div>)}

          {/* Data delete data props */}
          {mutateDeleteTodo.isLoading && "Delete data on progress.."}
          {mutateDeleteTodo.isError && (<div className='message'>{mutateDeleteTodo.error.message}</div>)}
          {mutateDeleteTodo.isSuccess && (<div className='message'>Data Removed</div>)}
          <TodoList list={data} markComplete={markComplete} delTodo={delTodo} />
        </div>
      )}
      <ReactQueryDevtools initialIsOpen={false} />
    </div>
  );
}

export default App;
