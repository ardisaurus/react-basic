import { useState } from "react";

const AddTodo= ({ addTodo }) => {
  const [state, setState] = useState("");

  const handleChange = (event) => {
    setState(event.target.value);
  };

  const onSubmit = (e) => {
    e.preventDefault();
    addTodo(state);
    setState("");
  };

  return (
    <>
      <form style={{ display: "flex" }} onSubmit={onSubmit}>
        <input
          type="text"
          name="title"
          style={{ flex: "10", padding: "5px" }}
          placeholder="Add Todo..."
          value={state}
          onChange={handleChange}
        />
        <input
          type="submit"
          value="Submit"
          className="btn"
          style={{ flex: 1 }}
        />
      </form>
    </>
  );
};

export default AddTodo;
