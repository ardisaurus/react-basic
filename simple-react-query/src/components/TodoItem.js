import "../App.css";

const TodoItem = ({
  todo,
  markComplete,
  delTodo,
}) => {
  return (
    <div className={todo.completed ? "todo-item-completed" : "todo-item"}>
      <p>
        <input
          type="checkbox"
          checked={todo.completed}
          onChange={() => markComplete(todo.id)}
        />
        {" " + todo.title}
        <button onClick={() => delTodo(todo.id)} className="btn-delete">
          X
        </button>
      </p>
    </div>
  );
};
export default TodoItem;
