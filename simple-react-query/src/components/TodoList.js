import TodoItem from "./TodoItem";

const TodoList = ({
  list,
  markComplete,
  delTodo,
}) => {
  return (
    <>
      {list.map((item) => (
        <TodoItem
          key={item.id}
          todo={item}
          markComplete={markComplete}
          delTodo={delTodo}
        />
      ))}
    </>
  );
};

export default TodoList;
