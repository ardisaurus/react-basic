import axios from 'axios';

const baseUrl = "https://jsonplaceholder.typicode.com/todos";

const api = axios.create({ baseURL: `${baseUrl}?_limit=10` });

export const getTodos = async () => { const res = await api.get(); return res.data };

export const addTodos = async (values) => { const res = await api.post(baseUrl, values); return res.data };

export const removeTodos = async (id) => { const res = await api.delete(`${baseUrl}/${id}`); return res.data };