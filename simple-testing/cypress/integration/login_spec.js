describe("Login", () => {
  it("user can login", () => {
    cy.visit("http://localhost:3000/");
    cy.findByRole("textbox", { name: /email/i }).type("admin@mail.com");
    cy.findByLabelText(/password/i).type("123456");
    cy.findByRole("button", { name: /log in/i }).click();
    cy.findByRole("heading", { name: /welcome/i }).should("be.visible");
  });
});
