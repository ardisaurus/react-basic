import LoginForm from "./components/LoginForm";
import WelcomeMessage from "./components/WelcomeMessage";
import Grid from "@material-ui/core/Grid";
import React from "react";

function App() {
  const [isLoggedIn, setIsLoggedIn] = React.useState(false);

  return (
    <Grid container justifyContent="center">
      {isLoggedIn && (
        <WelcomeMessage handleLogout={() => setIsLoggedIn(false)} />
      )}
      {!isLoggedIn && <LoginForm handleLogin={() => setIsLoggedIn(true)} />}
    </Grid>
  );
}

export default App;
