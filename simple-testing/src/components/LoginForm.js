import React from "react";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Alert from "@material-ui/lab/Alert";
import { useFormik } from "formik";
import * as Yup from "yup";

const validationSchema = Yup.object().shape({
  email: Yup.string().required("Insert email"),
  password: Yup.string()
    .min(6, "Use combination of 6 character or more")
    .required("Insert Password"),
});

export default function LoginForm({ handleLogin }) {
  const [isError, setIsError] = React.useState(false);
  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: validationSchema,
    validateOnBlur: false,
    validateOnChange: false,
    onSubmit: async ({ email, password }) => {
      if (email === "admin@mail.com" && password === "123456") {
        setIsError(false);
        handleLogin();
      } else {
        setIsError(true);
      }
    },
  });

  return (
    <Paper elevation={3} style={{ padding: "1em", width: "450px" }}>
      <Grid container direction="column" spacing={1}>
        <Grid item>
          <Typography variant="h4">Log In</Typography>
        </Grid>
        {isError && (
          <Alert severity="error" style={{ margin: "0.5em 0" }}>
            Use email : admin@mail.com and password : 123456 to login
          </Alert>
        )}
        <form onSubmit={formik.handleSubmit}>
          <Grid item>
            <TextField
              id="email"
              name="email"
              type="text"
              value={formik.values.email}
              onChange={formik.handleChange}
              variant="outlined"
              margin="dense"
              fullWidth
              label="email"
              error={Boolean(formik.errors.email) && formik.touched.email}
              helperText={formik.errors.email}
            />
          </Grid>
          <Grid item>
            <TextField
              id="password"
              name="password"
              type="password"
              value={formik.values.password}
              onChange={formik.handleChange}
              variant="outlined"
              margin="dense"
              fullWidth
              label="password"
              error={Boolean(formik.errors.password) && formik.touched.password}
              helperText={formik.errors.password}
            />
          </Grid>
          <Grid item>
            <Button
              id="submit"
              variant="contained"
              color="primary"
              fullWidth
              type="submit"
            >
              Log In
            </Button>
          </Grid>
        </form>
      </Grid>
    </Paper>
  );
}
