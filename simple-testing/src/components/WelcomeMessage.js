import React from "react";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

export default function WelcomeMessage({ handleLogout }) {
  return (
    <Grid container direction="column" justifyContent="center">
      <Grid item>
        <Typography variant="h1">Welcome</Typography>
      </Grid>
      <Grid item>
        <Button
          variant="contained"
          color="secondary"
          onClick={() => handleLogout()}
        >
          Log Out
        </Button>
      </Grid>
    </Grid>
  );
}
