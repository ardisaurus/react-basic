import { Link } from "react-router-dom";
import "../App.css";

export default function Header() {
  return (
    <div>
      <header className="header">
        <h1>Todo List</h1>
        <Link to="/" className="link">
          Home
        </Link>{" "}
        |{" "}
        <Link to="/about" className="link">
          About
        </Link>
      </header>
    </div>
  );
}
