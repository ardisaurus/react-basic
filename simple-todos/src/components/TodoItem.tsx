import { FunctionComponent } from "react";
import { ITodo } from "../pages/Home";
import "../App.css";

type TodoItemProps = {
  todo: ITodo;
  markComplete: (id: string) => void;
  delTodo: (id: string) => Promise<void>;
};

const TodoItem: FunctionComponent<TodoItemProps> = ({
  todo,
  markComplete,
  delTodo,
}) => {
  return (
    <div className={todo.completed ? "todo-item-completed" : "todo-item"}>
      <p>
        <input
          type="checkbox"
          checked={todo.completed}
          onChange={() => markComplete(todo.id)}
        />
        {" " + todo.title}
        <button onClick={() => delTodo(todo.id)} className="btn-delete">
          X
        </button>
      </p>
    </div>
  );
};
export default TodoItem;
