import { FunctionComponent } from "react";
import TodoItem from "./TodoItem";
import { ITodo } from "../pages/Home";

type TodoListProps = {
  list: ITodo[];
  markComplete: (id: string) => void;
  delTodo: (id: string) => Promise<void>;
};

const TodoList: FunctionComponent<TodoListProps> = ({
  list,
  markComplete,
  delTodo,
}) => {
  return (
    <>
      {list.map((item: ITodo) => (
        <TodoItem
          key={item.id}
          todo={item}
          markComplete={markComplete}
          delTodo={delTodo}
        />
      ))}
    </>
  );
};

export default TodoList;
