import { useState } from "react";
import TodoList from "../components/TodoList";
import AddTodo from "../components/AddTodo";
import { v4 as uuid } from "uuid";
import { todoInitValue } from "../constant/initValue";

export interface ITodo {
  id: string;
  completed: boolean;
  title: string;
}

export default function Home() {
  const [todoList, setTodoList] = useState<ITodo[]>(todoInitValue);

  const markComplete = (id: string) => {
    const res = todoList.map((todo: ITodo) => {
      if (todo.id === id) {
        todo.completed = !todo.completed;
      }
      return todo;
    });
    setTodoList(res);
  };

  const delTodo = async (id: string) => {
    const res = todoList.filter((todo) => todo.id !== id);
    setTodoList(res);
  };

  const addTodo = async (title: string) => {
    setTodoList([{ title, completed: false, id: uuid() }, ...todoList]);
  };

  return (
    <>
      <AddTodo addTodo={addTodo} />
      <TodoList list={todoList} markComplete={markComplete} delTodo={delTodo} />
    </>
  );
}
